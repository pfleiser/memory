# README #

### What is this repository for? ###

* This repository is for homework, made by Pavel Fleiser.
* Here you can find code of a program called 'Memory Game'.
* Author used next tutorials and sources for his work:
  Java Documentation ([Link](http://docs.oracle.com/javase/8/javase-clienttechnologies.htm)), Tutorial for creating Memory Game ([Link](https://www.youtube.com/watch?v=QjuytZhQYo8))
* Licence: GPL ([Link](Link URL))
  
### Instruction ###

* To run this program, use Eclipse and mouse.
* Rules of the game: Before you have a field with closed squares, which are hidden numbers. You have only 15 seconds to find a pair of numbers that you have opened. If you find a pair, the timer starts counting again. If not, then the game is lost. Every time you choose the wrong bet you lose 1 point. Good luck!


### Who do I talk to? ###

* Repo owner or admin: Pavel Fleiser, [pfleiser@itcollege.ee](mailto:pfleiser@itcollege.ee)
