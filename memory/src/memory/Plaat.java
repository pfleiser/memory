package memory;

import javafx.animation.FadeTransition;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Duration;

// StackPane and pane feature used from tutorial

public class Plaat extends StackPane {
	private String value;
    private Text text = new Text();
    private Rectangle piir = new Rectangle(45, 45);
    
    public Plaat(String value) {
    	this.value = value;
        piir.setStroke(Color.PURPLE);
        
        text.setText(value);
        text.setVisible(false);

        setAlignment(Pos.CENTER); // alignment of tiles and characters
        getChildren().addAll(piir, text);

        setOnMouseClicked(this::klikijuhtimine); // counts reached zero
        close();
    }

    public void klikijuhtimine (MouseEvent event) {
        if (isOpen() || MemoryGame.klikiloendur == 0) // 
            return;
        
        
        if (MemoryGame.valitud == null) { 
        	MemoryGame.valitud = this; // we assign this tile to selected and...
            open(() -> {}); // let now program know which tile is already opened
        } 
        else {
            open(() -> { 
            	if (!SamaV22rtus(MemoryGame.valitud)) {
            		MemoryGame.valitud.close(); // if it's not the same value, so program should close the tile
                	MemoryGame.scoreText.setText("SCORE: " + (Integer.valueOf(MemoryGame.scoreText.getText().substring(7)) - 1)); // if pair not found -> score - 1
                    this.close();
                } else {
                	MemoryGame.plaadid.remove(this);
                	MemoryGame.plaadid.remove(MemoryGame.valitud);
                	if(MemoryGame.plaadid.size() == 0) MemoryGame.gameOver(true);
                	MemoryGame.scoreText.setText("SCORE: " + (Integer.valueOf(MemoryGame.scoreText.getText().substring(7)) + 5)); // if pair found -> score + 5
                	MemoryGame.startTimer(); // got a pair -> reset the timer
                }
            	MemoryGame.valitud = null;
            	MemoryGame.klikiloendur = 2;
            });
        }
    }

    public boolean isOpen() {
        return text.getOpacity() == 1; // checks if tile is open or not
    }

    public void open(Runnable action) {
        FadeTransition transition = new FadeTransition(Duration.seconds(0.1), text); // fade-out effect for tile during opening
        transition.setToValue(1);
        transition.setOnFinished(e -> action.run());
        transition.play();
        piir.setFill(new ImagePattern(new Image(getClass().getResourceAsStream("/pictures/" + value + ".png"))));
    }

    public void close() {
        FadeTransition transition = new FadeTransition(Duration.seconds(0.1), text); // fade-in effect for tile during closing
        transition.setToValue(0);
        transition.play();
        piir.setFill(new ImagePattern(new Image(getClass().getResourceAsStream("/pictures/square.png"))));
    }
    

    public boolean SamaV22rtus(Plaat other) {
        return text.getText().equals(other.text.getText()); // returns true if tiles are the same values
    }
}