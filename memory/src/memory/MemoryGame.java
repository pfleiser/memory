//
// Memory Game
// author: @pfleiser
// version: 1.02
//

package memory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.paint.Color;
import javafx.scene.effect.*;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;

public class MemoryGame extends Application {
	protected static List<Plaat> plaadid = new ArrayList<>();
    private static Timer timer = new Timer(); // timer which counts seconds and clicks... yeah!
    private static int secCount;
    protected static int klikiloendur = 2;
    protected static Pane root = new Pane();
    private static Text timeText = new Text();
    protected static Text scoreText = new Text();
    protected static Text rulesText = new Text();
    protected static Text rulesTitle = new Text();
    protected static Text levelTitle = new Text();
    protected static Text gameTitle = new Text();
    protected static Plaat valitud = null;
    private static int difficulty;
    JPanel contentPane;
    JLabel imageLabel = new JLabel();
    
    private static int paarideNumber; // number of tile pairs
    private static int reaNumber; // number of tiles per row
    
	private static void createContent() {
		switch (difficulty) {
		case 0:
			paarideNumber = 18;
			reaNumber = 20;
			break;
		case 1:
			paarideNumber = 38;
			reaNumber = 20;
			break;
		case 2:
			paarideNumber = 70;
			reaNumber = 20;
			break;
		}
		int index = 0;
		for (int i = 0; i < paarideNumber; i++) { // number of pairs
			plaadid.add(new Plaat(String.valueOf(index))); 
			plaadid.add(new Plaat(String.valueOf(index)));
			index = index == 7 ? 0 : index + 1;
		}
		Collections.shuffle(plaadid);
		for (int i = 0; i < plaadid.size(); i++) { // just creates cycle for shuffling
			Plaat Plaat = plaadid.get(i);
			Plaat.setTranslateY(50 * (i / reaNumber)); // placing the tile
			Plaat.setTranslateX(50 * (i % reaNumber));
			root.getChildren().add(Plaat); // placing tile in the scene graph and showing on the screen
		}
		Button resetButton = new Button("RESET");
		resetButton.setScaleX(1.75); // size X
		resetButton.setScaleY(1.75); // size Y
		resetButton.setLayoutX(200); // position x
		resetButton.setLayoutY(365); // position y
		resetButton.setOnAction(event -> newGame());
		root.getChildren().add(resetButton);
		timeText.setLayoutX(350);
		timeText.setLayoutY(400);
		timeText.setFont(Font.font(60));
		timeText.setFill(Color.WHITE);
		root.getChildren().add(timeText);
		scoreText.setLayoutX(470);
		scoreText.setLayoutY(390);
		scoreText.setFont(Font.font(30));
		scoreText.setText("SCORE: 0");
		scoreText.setFill(Color.WHITE);
		root.getChildren().add(scoreText);
		
		Button homeButton = new Button("HOME");
		homeButton.setScaleX(1.75); // size X
		homeButton.setScaleY(1.75); // size Y
		homeButton.setLayoutX(50); // position x
		homeButton.setLayoutY(365); // position y
		homeButton.setOnAction(event ->  {timer.cancel(); root.getChildren().clear(); setWelcomeScreen();});
		root.getChildren().add(homeButton);
	}
	
    protected static void startTimer() {
    	timeText.setText("15");
		timer.cancel();
    	secCount = 0;
    	timer = new Timer();
    	timer.schedule(new TimerTask() {
    		@Override // link for method from object
    		public void run() {
    			timeText.setText(String.valueOf(Integer.valueOf(timeText.getText()) - 1));
    			if(++secCount == 15) gameOver(false);
    		}
    	}, 1000, 1000); // executing timer task after 1000ms every 1000ms
    }

	protected static void gameOver(boolean won) {
		timer.cancel();
		root.setDisable(true);
	    JOptionPane.showMessageDialog(null, (won ? "You won! " : "Game over, bro!") + " " + scoreText.getText() + "\nPlay again?"); // dialog screen appears
	    Platform.runLater(() -> newGame()); // button OK starts new game on the same level
	}
    
    private static void newGame() { // method which clears the field after reset
		timer.cancel();
		root.setDisable(false);
		root.getChildren().clear(); // removing all from root
		plaadid.clear();
    	createContent(); // adding everything to root
        startTimer(); // game begin -> timer start running
    }
	
    @Override
    public void start(Stage peamine) throws Exception {
        root.setPrefSize(800, 430); // setting size of the window
        root.setBackground(new Background(
				new BackgroundImage(new Image(getClass().getResourceAsStream("/pictures/bg_main.gif")),
				BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
        
        peamine.setScene(new Scene(root));
        peamine.setTitle("Memory Game"); // the window title
        peamine.setOnCloseRequest(event -> timer.cancel()); // close window -> canceling the timer
        peamine.show();
    	setWelcomeScreen();
    }
    
    
    private static void setWelcomeScreen() { // setting Welcome Screen with level buttons
    	String[] difficulties = {"EASY", "MEDIUM", "HARD"};
		for (int i = 0; i < 3; i++) {
	        Button startButton = new Button(difficulties[i]);
	        final int fin = i;
			startButton.setOnAction(event -> {difficulty = fin; newGame();});
			startButton.setScaleX(1.75); 
			startButton.setScaleY(1.75); 
			startButton.setLayoutX(i*150 + 200);
			if(i == 2) startButton.setLayoutX(startButton.getLayoutX() + 20);
			startButton.setLayoutY(365);
			root.getChildren().add(startButton);
		}
	
		DropShadow ds2 = new DropShadow(); // title for Welcome Screen
        ds2.setOffsetY(3.0f);
        ds2.setColor(Color.color(0.7f, 0.7f, 0.7f));
		gameTitle.setEffect(ds2);
		gameTitle.setLayoutX(180);
		gameTitle.setLayoutY(80);
		gameTitle.setFont(Font.font ("Ubuntu", FontWeight.BOLD, 55));
		gameTitle.setText("MEMORY GAME");
		gameTitle.setFill(Color.WHITE);
		root.getChildren().add(gameTitle);
		
		DropShadow ds = new DropShadow();
        ds.setOffsetY(3.0f);
        ds.setColor(Color.color(0.7f, 0.7f, 0.7f));
		rulesTitle.setEffect(ds);
		rulesTitle.setLayoutX(350);
		rulesTitle.setLayoutY(130);
		rulesTitle.setFont(Font.font("Ubuntu Light", FontWeight.BOLD, 30));
		rulesTitle.setText("Rules");
		rulesTitle.setFill(Color.LIGHTGRAY);
		root.getChildren().add(rulesTitle);		
		
        DropShadow ds1 = new DropShadow(); // rules text on Welcome Screen
        ds1.setOffsetY(3.0f);
        ds1.setColor(Color.color(0.9f, 0.9f, 0.9f));
		rulesText.setEffect(ds1);
		rulesText.setCache(true);
		rulesText.setLayoutX(175);
		rulesText.setLayoutY(150);
		rulesText.setFont(Font.font("Ubuntu Light", FontWeight.BOLD, 15));
		rulesText.setText("Before you have a field with closed squares, which are hidden \n"
				+ "numbers. You have only 15 seconds to find a pair of numbers that \n"
				+ "you have opened. If you find a pair, the timer starts counting \n"
				+ "again. If not, then the game is lost. Every time you choose the \n"
				+ "wrong bet you lose 1 point. Good luck! \n");
		rulesText.setFill(Color.LIGHTGRAY);
		root.getChildren().add(rulesText);
		
		DropShadow ds11 = new DropShadow();
        ds11.setOffsetY(3.0f);
        ds11.setColor(Color.color(0.7f, 0.7f, 0.7f));
        levelTitle.setEffect(ds11);
        levelTitle.setLayoutX(50);
        levelTitle.setLayoutY(375);
        levelTitle.setFont(Font.font(15));
        levelTitle.setText("Choose \n"
        		+ "your level:");
        levelTitle.setFill(Color.WHITE);
		root.getChildren().add(levelTitle);
	}

	public static void main(String[] args) {
        Application.launch(args);
    }
}